use thiserror::Error;

#[derive(Error, Debug)]
pub enum QueueError {
  #[error("lapin: `{0}`")]
  Lapin(#[from] lapin::Error),
  #[error("nack: `{0}`")]
  Nack(String),
  #[error("serde_json: `{0}`")]
  SerdeJson(#[from] serde_json::Error),
}
