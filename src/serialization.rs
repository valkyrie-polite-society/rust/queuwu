use serde::Serialize;
use crate::QueueError;

pub enum Serialization {
  Json,
}

impl Default for Serialization {
  fn default() -> Self {
    self::Serialization::Json
  }
}

pub fn serialize<TPublish: Serialize>(message: TPublish, format: &Serialization) -> Result<Vec<u8>, QueueError> {
  match format {
    Serialization::Json => Ok(serde_json::to_vec(&message)?)
  }
}
