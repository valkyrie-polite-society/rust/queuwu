use async_trait::async_trait;
use lapin::Connection;
use lapin::ConnectionProperties;
use serde::Serialize;
use serde::Deserialize;

use crate::Serialization;
use crate::ConsumeContext;
use crate::Consumer;
use crate::error::QueueError;

#[tokio::test]
async fn can_publish() {
  let connection = Connection::connect(
    "amqp://127.0.0.1:5672/%2f",
    ConnectionProperties::default().with_default_executor(8),
  ).await.unwrap();

  let channel = connection.create_channel().await.unwrap();

  let context = ConsumeContext{
    message: TestConsume,
    channel,
    serialization: Serialization::Json
  };

  context.publish(TestPublish{
    c: "aaaa".into(),
    d: "bbbb".into()
  }).await.unwrap();
}

#[derive(Deserialize)] struct TestConsume;

struct TestConsumer;

#[async_trait]
impl Consumer<TestConsume> for TestConsumer {
  async fn consume(_context: ConsumeContext<TestConsume>) -> Result<(), QueueError> {
    Ok(())
  }
}

#[derive(Serialize)]
struct TestPublish {
  pub c: String,
  pub d: String
}

