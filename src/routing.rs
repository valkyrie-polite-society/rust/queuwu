use lapin::Channel;
use lapin::options::ExchangeDeclareOptions;
use lapin::options::QueueBindOptions;
use lapin::options::QueueDeclareOptions;
use lapin::types::FieldTable;

use crate::error::QueueError;

pub async fn get_publish_routing<T>(channel: &Channel) -> Result<(String, String), QueueError> {
  let type_name: String = std::any::type_name::<T>().into();

  let (exchange, queue) = (type_name, String::new());

  channel.exchange_declare(
    &exchange,
    lapin::ExchangeKind::Fanout,
    ExchangeDeclareOptions::default(),
    FieldTable::default()
  ).await?;
    
  channel.queue_declare(
    &exchange,
    QueueDeclareOptions::default(),
    FieldTable::default(),
  ).await?;

  channel.queue_bind(
    &exchange,
    &exchange,
    "",
    QueueBindOptions::default(),
    FieldTable::default()
  ).await?;

  Ok((exchange, queue))
}
