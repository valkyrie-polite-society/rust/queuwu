use async_trait::async_trait;
use lapin::BasicProperties;
use lapin::Channel;
use lapin::options::BasicPublishOptions;
use lapin::publisher_confirm::Confirmation::Nack;
use serde::Serialize;
use serde::de::DeserializeOwned;
use crate::error::QueueError;
use crate::serialization::*;

mod error;
mod routing;
mod serialization;

#[cfg(test)] mod tests;

pub struct ConsumeContext<TMessage: DeserializeOwned> {
  pub message: TMessage,
  channel: Channel,
  serialization: Serialization
}

#[async_trait]
pub trait Consumer<TMessage: DeserializeOwned>
{
  async fn consume(context: ConsumeContext<TMessage>) -> Result<(), QueueError>;
}

impl <TMessage: DeserializeOwned> ConsumeContext<TMessage> {
  pub async fn publish<TPublish: Serialize>(&self, message: TPublish) -> Result<(), QueueError> {
    let payload = serialize(message, &self.serialization)?;
    let (exchange, queue) = self::routing::get_publish_routing::<TPublish>(&self.channel).await?;
    let confirmation = self.channel.basic_publish(
      &exchange,
      &queue,
      BasicPublishOptions::default(),
      payload,
      BasicProperties::default()
    ).await?
    .await?;

    match confirmation {
      Nack(_) => Err(QueueError::Nack("Nack".into())),
      _       => Ok(())
    }
  }
}
